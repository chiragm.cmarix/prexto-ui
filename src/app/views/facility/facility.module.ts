import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FacilityRoutingModule } from './facility-routing.module';
import { RequestComponent } from './request/request.component';
import { FacilityComponent } from './facility.component';
import { AccountComponent } from './account/account.component';
import { AllocationComponent } from './allocation/allocation.component';


@NgModule({
  declarations: [
    RequestComponent,
    FacilityComponent,
    AccountComponent,
    AllocationComponent
  ],
  imports: [
    CommonModule,
    FacilityRoutingModule
  ]
})
export class FacilityModule { }
