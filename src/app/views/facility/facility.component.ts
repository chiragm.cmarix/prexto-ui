import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-facility',
  templateUrl: './facility.component.html',
  styleUrls: ['./facility.component.scss']
})
export class FacilityComponent implements OnInit {

  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
  }

  currDiv: string = 'A';

  ShowDiv(divVal: string) {
    // debugger;
    this.currDiv = divVal;
    this.changeDetectorRef.detectChanges();
  }

}
