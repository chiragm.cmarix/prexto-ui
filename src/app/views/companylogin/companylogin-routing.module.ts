import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompanyloginComponent } from './companylogin.component';

const routes: Routes = [
  {
    path: '',
    component: CompanyloginComponent,
    data: {
      title: 'MyPerformance'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyloginRoutingModule { }
