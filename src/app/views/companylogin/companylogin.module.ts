import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { CompanyloginRoutingModule } from './companylogin-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatFormFieldModule,
    CompanyloginRoutingModule
  ]
})

export class CompanyloginModule {
    
}
