import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyPerformanceRoutingModule } from './my-performance-routing.module';
import { MyPerformanceComponent } from './my-performance.component';
import { PaymentComponent } from './payment/payment.component';
import { EcosystemComponent } from './ecosystem/ecosystem.component';
import { CreditComponent } from './credit/credit.component';
import { RiskindicatorComponent } from './riskindicator/riskindicator.component';
import { ExperianceComponent } from './experiance/experiance.component';
import { MarketingComponent } from './marketing/marketing.component';



@NgModule({
  imports: [
    CommonModule,
    MyPerformanceRoutingModule
  ],
  declarations: [
    MyPerformanceComponent,
    PaymentComponent,
    EcosystemComponent,
    CreditComponent,
    RiskindicatorComponent,
    ExperianceComponent,
    MarketingComponent,
  ]
})
export class MyPerformanceModule { }
