import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-performance',
  templateUrl: './my-performance.component.html',
  styleUrls: ['./my-performance.component.scss']
})

export class MyPerformanceComponent implements OnInit {

  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
  }

  currDiv: string = 'A';

  ShowDiv(divVal: string) {
    // debugger;
    this.currDiv = divVal;
    this.changeDetectorRef.detectChanges();
  }

}
