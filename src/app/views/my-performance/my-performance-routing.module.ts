import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyPerformanceComponent } from './my-performance.component';

const routes: Routes = [
  {
    path: '',
    component: MyPerformanceComponent,
    data: {
      title: 'MyPerformance'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPerformanceRoutingModule {}
