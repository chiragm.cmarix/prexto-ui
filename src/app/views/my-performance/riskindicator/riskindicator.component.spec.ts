import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskindicatorComponent } from './riskindicator.component';

describe('RiskindicatorComponent', () => {
  let component: RiskindicatorComponent;
  let fixture: ComponentFixture<RiskindicatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RiskindicatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskindicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
