import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MarketplaceComponent } from '../marketplace/marketplace.component';
import { AddserviceComponent } from './addservice/addservice.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
  {
    path:'',
    component:  MarketplaceComponent,
    data:{
      title: 'MyPerformance'
    }
  },
  {
    path:'edit',
    component:  EditComponent,
    data:{
      title: 'MyPerformance - edit'
    }
  },
  {
    path:'create',
    component:  AddserviceComponent,
    data:{
      title: 'MyPerformance - Add Service'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketplaceRoutingModule { }
