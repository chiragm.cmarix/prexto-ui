import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarketplaceRoutingModule } from './marketplace-routing.module';
import { AddserviceComponent } from './addservice/addservice.component';
import { EditComponent } from './edit/edit.component';


@NgModule({
  declarations: [
    AddserviceComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    MarketplaceRoutingModule
  ]
})
export class MarketplaceModule { }
