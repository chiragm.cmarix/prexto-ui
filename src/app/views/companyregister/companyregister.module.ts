import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyregisterRoutingModule } from './companyregister-routing.module';
import { CompanyregisterComponent } from './companyregister.component';


@NgModule({
  declarations: [
    CompanyregisterComponent
  ],
  imports: [
    CommonModule,
    CompanyregisterRoutingModule
  ]
})


export class CompanyregisterModule { }
