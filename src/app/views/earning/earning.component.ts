import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-earning',
  templateUrl: './earning.component.html',
  styleUrls: ['./earning.component.scss']
})
export class EarningComponent implements OnInit {

  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
  }


  currDiv: string = 'A';

  ShowDiv(divVal: string) {
    // debugger;
    this.currDiv = divVal;
    this.changeDetectorRef.detectChanges();
  }

}
