import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EarningRoutingModule } from './earning-routing.module';
import { EarningComponent } from './earning.component';
import { LoansComponent } from './loans/loans.component';
import { ReportComponent } from './report/report.component';


@NgModule({
  declarations: [
    EarningComponent,
    LoansComponent,
    ReportComponent
  ],
  imports: [
    CommonModule,
    EarningRoutingModule
  ]
})
export class EarningModule { }
