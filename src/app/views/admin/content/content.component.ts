import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FileValidators } from "ngx-file-drag-drop";
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']

})

export class ContentComponent implements OnInit {

  fileControl = new FormControl(
    [],
    [FileValidators.required,
    FileValidators.maxFileCount(2)]
  );
  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) { }


  // public files: NgxFileDropEntry[] = [];

  // public dropped(files: NgxFileDropEntry[]) {
  //   this.files = files;
  //   for (const droppedFile of files) {

  //     // Is it a file?
  //     if (droppedFile.fileEntry.isFile) {
  //       const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
  //       fileEntry.file((file: File) => {

  //         // Here you can access the real file
  //         console.log(droppedFile.relativePath, file);

  //       });
  //     } else {
  //       // It was a directory (empty directories are added, otherwise only files)
  //       const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
  //       console.log(droppedFile.relativePath, fileEntry);
  //     }
  //   }
  // }

  public fileOver(event){
    console.log(event);
  }

  public fileLeave(event){
    console.log(event);
   
  } 
  
  
  onValueChange(file: File[]) {
    console.log("File changed!");
  }
  
  ngOnInit(): void {
    this.fileControl.valueChanges.subscribe((files: File[]) =>
      console.log(this.fileControl.value, this.fileControl.valid)
    );
  }

  currDiv: string = 'A';

  ShowDiv(divVal: string) {
    // debugger;
    this.currDiv = divVal;
    this.changeDetectorRef.detectChanges();
  }


  files: File[] = [];

onSelect(event) {
  console.log(event);
  this.files.push(...event.addedFiles);
}

onRemove(event) {
  console.log(event);
  this.files.splice(this.files.indexOf(event), 1);
}
}


