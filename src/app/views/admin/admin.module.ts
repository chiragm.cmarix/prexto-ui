import { NgModule,OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { FormsModule } from '@angular/forms';
import { DashboardRoutingModule } from '../dashboard/dashboard-routing.module';
import { ChartsModule } from 'ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AdminRoutingModule } from './admin-routing.module';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ContentComponent } from './content/content.component';
import { LegalComponent } from './legal/legal.component';
import { NgxFileDragDropModule } from "ngx-file-drag-drop";
import { CouponComponent } from './coupon/coupon.component';
import { ReportComponent } from './report/report.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SubscriptionComponent } from './subscription/subscription.component';
import { NotificationComponent } from './notification/notification.component';
import { ProfileComponent } from './profile/profile.component';
import { AdduserComponent } from './adduser/adduser.component';
import { PermissionComponent } from './permission/permission.component';
import { BillingComponent } from './billing/billing.component';
import { NgxTTitanColorPickerModule } from 'ngx-ttitan-color-picker';



@NgModule({
  
  imports: [
    AdminRoutingModule,
    NgxTTitanColorPickerModule,
    TabsModule,
    CommonModule,
    NgxFileDragDropModule,
    NgxDropzoneModule
  ],
  declarations: [ AdminComponent, ContentComponent, LegalComponent, CouponComponent, ReportComponent, SubscriptionComponent, NotificationComponent, ProfileComponent, AdduserComponent, PermissionComponent, BillingComponent ]
})




export class AdminModule { }
