import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FileValidators } from "ngx-file-drag-drop";
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  changeDetectorRef: any;

 
  fileControl = new FormControl(
    [],
    [FileValidators.required,
    FileValidators.maxFileCount(2)]
  );
  constructor(
  ) { }

  public fileOver(event){
    console.log(event);
  }

  public fileLeave(event){
    console.log(event);
   
  } 
  
  onValueChange(file: File[]) {
    console.log("File changed!");
  }

  ngOnInit(): void {
    this.fileControl.valueChanges.subscribe((files: File[]) =>
    console.log(this.fileControl.value, this.fileControl.valid)
  );
  }


  currDiv: string = 'A';

  ShowDiv(divVal: string) {
    // debugger;
    this.currDiv = divVal;
    this.changeDetectorRef.detectChanges();
  }

  files: File[] = [];

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }



}
