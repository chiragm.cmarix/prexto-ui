import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  clicked = false;
  bsModalRef:any= BsModalRef;
  storagedata = null;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(public modalRef: BsModalRef,) { }

  ngOnInit(): void {
  }

}
