import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { CompanyloginComponent } from './views/companylogin/companylogin.component';
import { CompanyregisterComponent } from './views/companyregister/companyregister.component';
import { ErrorComponent } from './views/error/error.component';

// import { P404Component } from './views/error/404.component';

import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
 
  {
    path: 'companylogin',
    component: CompanyloginComponent,
    data: {
      title: 'companylogin'
    }
  },

  {
    path: 'companyregister',
    component: CompanyregisterComponent,
    data: {
      title: 'companyregister'
    }
  },

  {
    path: 'error',
    component: ErrorComponent,
    data: {
      title: 'error Page'
    }
  },
  
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'admin',
        loadChildren: () => import('./views/admin/admin.module').then(m => m.AdminModule)
      },
      {
        path: 'performance',
        loadChildren: () => import('./views/my-performance/my-performance.module').then(m => m.MyPerformanceModule)
      },
      {
        path: 'earning',
        loadChildren: () => import('./views/earning/earning.module').then(m => m.EarningModule)
      },
      {
        path: 'facility',
        loadChildren: () => import('./views/facility/facility.module').then(m => m.FacilityModule)
      },
      {
        path: 'marketplace',
        loadChildren: () => import('./views/marketplace/marketplace.module').then(m => m.MarketplaceModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
